## Repository URL
 https://gitlab.com/kyungjoongo/ball_count_test

## Framework used (Library)
    "react": "17.0.1", 

##  How to run the project.
    yarn && yarn start
    
## Live demo
    https://ball-count.web.app/