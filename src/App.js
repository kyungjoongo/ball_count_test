// @flow
import './App.css';
import * as React from 'react';

const balls = [
    {
        name: 'red',
        img: 'https://cdn0.iconfinder.com/data/icons/classic-icons/512/090.png',

    },
    {
        name: 'blue',
        img: 'https://cdn2.iconfinder.com/data/icons/kick-off/450/Football-Blue-512.png',
    },
]


type Props = {};
type State = {
    randNo: number,
    redViewCount: number,
    blueViewCount: number,

};

export const BALL_INDEX = 'ballIndex';
export const BALL_VIEW_COUNT = 'ballViewCount';


export default class App extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            randNo: 0,
            redViewCount: 0,
            blueViewCount: 0,
        }
    }

    componentDidMount() {
        this.calcBallViewCount();
    }


    rand(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    createCookie(cookieName, cookieValue, hourToExpire = 1) {
        let date = new Date();
        date.setTime(date.getTime() + (hourToExpire * 60 * 60 * 1000));
        document.cookie = cookieName + " = " + cookieValue + "; expires = " + date.toGMTString();
    }

    readCookie(name) {
        let nameEQ = name + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }

    eraseCookie(name) {
        this.createCookie(name, "", -1);
    }

    calcBallViewCount() {
        let ballIndex = this.readCookie(BALL_INDEX)

        //todo: If the cookie is not set.
        if (ballIndex === null) {
            this.setState({
                randNo: this.rand(0, 1),
            }, () => {
                this.createCookie(BALL_INDEX, this.state.randNo, 1)
                this.createCookie(BALL_VIEW_COUNT, 1, 1)

                this.setState({
                    redViewCount: this.state.randNo === 0 && 1,
                    blueViewCount: this.state.randNo === 1 && 1,
                })
            })
        } else {//todo: Bringing previously saved cookies

            let preViewCount = this.readCookie(BALL_VIEW_COUNT);
            let ballIndex = this.readCookie(BALL_INDEX);

            this.eraseCookie(BALL_VIEW_COUNT)
            this.createCookie(BALL_VIEW_COUNT, parseInt(preViewCount) + 1, 1);

            let currentViewCount = this.readCookie(BALL_VIEW_COUNT);
            this.setState({
                randNo: ballIndex,
                redViewCount: parseInt(ballIndex) === 0 && currentViewCount,
                blueViewCount: parseInt(ballIndex) === 1 && currentViewCount,
            })
        }

    }

    renderViewCountTable() {
        return (
            <table style={{border: 1, borderColor: 'black', borderStyle: 'dashed', width: '100%'}}>
                <thead style={{border: 1, borderColor: 'black', borderStyle: 'solid',}}>
                <th style={{background: 'red', fontSize: 22, color: 'white'}}>Red Ball View Count</th>
                <th style={{background: 'blue', fontSize: 22, color: 'white'}}>Blue Ball View Count</th>
                </thead>
                <tr style={{border: 1, borderColor: 'black', borderStyle: 'solid', textAlign: 'center'}}>
                    <td style={{fontSize: 30, fontWeight: 'bold'}}>
                        {this.state.redViewCount}
                    </td>
                    <td style={{fontSize: 30, fontWeight: 'bold'}}>
                        {this.state.blueViewCount}
                    </td>
                </tr>
            </table>
        )
    }

    render() {
        return (
            <div style={{margin: 0}}>

                <br/>
                <br/>
                <div className='center'>
                    <img alt={'ball'} src={balls[this.state.randNo].img}
                         style={{width: 50, height: 50}}/>
                </div>

                <br/>
                <br/>
                <br/>
                {this.renderViewCountTable()}

            </div>
        );
    };
};


